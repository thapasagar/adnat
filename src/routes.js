import React from "react";
import { Switch, Route, Link } from "react-router-dom";

import Layout from "./hoc/layout";

import Signup from "./components/Signup";
import Login from "./components/Login";
import Home from "./components/dashboard";

import Edit from "./components/dashboard/Edit";
import Shift from "./components/dashboard/Shift";
import Join from "./components/dashboard/Join";

import PrivateRoutes from "./components/authRoutes/privateRoute";

const DefaultPage = () => <div>home page</div>;
const Routes = () => {
  return (
    <Layout>
      <Switch>
        <Route path="/signup" exact component={Signup} />
        <Route path="/login" exact component={Login} />

        <PrivateRoutes
          restricted={true}
          path="/shifts"
          exact
          component={Shift}
        />

        <PrivateRoutes
          restricted={true}
          path="/edit/:id"
          exact
          component={Edit}
        />

        <PrivateRoutes
          restricted={true}
          path="/organisation/join/:id"
          exact
          component={Join}
        />

        <PrivateRoutes
          restricted={true}
          path="/organisation"
          exact
          component={Home}
        />

        <PrivateRoutes
          restricted={true}
          path="/"
          exact
          component={DefaultPage}
        />
      </Switch>
    </Layout>
  );
};

export default Routes;
