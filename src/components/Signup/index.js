import React, { Component } from "react";
import axios from "axios";

import FormField from "../utils/form/formField";
import { validate } from "../utils/form/misc";
import { update, generateData, isFormValid } from "../utils/form/formActions";
import { Link } from "react-router-dom";

class Signup extends Component {
  state = {
    formError: false,
    formSuccess: false,
    formdata: {
      name: {
        element: "input",
        value: "",
        config: {
          name: "name_input",
          type: "text",
          placeholder: "Enter your name",
          label: "Name"
        },
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        validationMessage: "",
        showlabel: true
      },

      email: {
        element: "input",
        value: "",
        config: {
          name: "email_input",
          type: "email",
          placeholder: "Enter your email",
          label: "Email"
        },
        validation: {
          required: true,
          email: true
        },
        valid: false,
        touched: false,
        validationMessage: "",
        showlabel: true
      },
      password: {
        element: "input",
        value: "",
        config: {
          name: "password_input",
          type: "password",
          placeholder: "Enter your password",
          label: "Password"
        },
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        validationMessage: "",
        showlabel: true
      },
      passwordConfirmation: {
        element: "input",
        value: "",
        config: {
          name: "confirm_password_input",
          type: "password",
          placeholder: "Confirm your password",
          label: "Password confirmation"
        },
        validation: {
          required: true,
          confirm: "password"
        },
        valid: false,
        touched: false,
        validationMessage: "",
        showlabel: true
      }
    },
    serverError: null
  };

  updateForm = element => {
    const newFormdata = update(element, this.state.formdata, "register");
    this.setState({
      formError: false,
      formdata: newFormdata
    });
  };

  submitForm = event => {
    event.preventDefault();

    let dataToSubmit = generateData(this.state.formdata, "register");
    let formIsValid = isFormValid(this.state.formdata, "register");

    if (formIsValid) {
      axios.post("/auth/signup", dataToSubmit).then(
        response => {
          localStorage.setItem("token", response.data.sessionId);
          localStorage.setItem("name", dataToSubmit.email);
          this.props.history.push("/organisation");
        },
        error => {
          this.setState({ serverError: "Something went wrong." });
        }
      );
    } else {
      this.setState({
        error: "Something went wrong."
      });
    }
  };

  render() {
    return (
      <div className="page_wrapper">
        <Link to="/">Adnat</Link>
        <div className="container">
          <div className="register_login_container">
            <div className="left">
              <form onSubmit={event => this.submitForm(event)}>
                <h2>Sign up</h2>
                <div className="form_block_two">
                  <div className="block">
                    <FormField
                      id={"name"}
                      formdata={this.state.formdata.name}
                      change={element => this.updateForm(element)}
                    />
                  </div>
                </div>
                <div>
                  <FormField
                    id={"email"}
                    formdata={this.state.formdata.email}
                    change={element => this.updateForm(element)}
                  />
                </div>

                <div className="form_block_two">
                  <div className="block">
                    <FormField
                      id={"password"}
                      formdata={this.state.formdata.password}
                      change={element => this.updateForm(element)}
                    />
                  </div>
                  <div className="block">
                    <FormField
                      id={"passwordConfirmation"}
                      formdata={this.state.formdata.passwordConfirmation}
                      change={element => this.updateForm(element)}
                    />
                  </div>
                </div>
                <div>
                  {this.state.formError ? (
                    <div className="error_label">Please check your data</div>
                  ) : null}
                  {this.state.serverError ? (
                    <div className="error_label">{this.state.serverError}</div>
                  ) : null}
                  <button onClick={event => this.submitForm(event)}>
                    Sign up
                  </button>
                </div>
              </form>
              <Link to="/login">Log in</Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Signup;
