import React, { Component } from "react";
import axios from "axios";
import { Link, Route } from "react-router-dom";
import FormField from "../utils/form/formField";
import { validate } from "../utils/form/misc";
import { update, generateData, isFormValid } from "../utils/form/formActions";
import Header from "../header/";

export class Home extends Component {
  state = {
    formError: false,
    formSuccess: "",
    formdata: {
      name: {
        element: "input",
        value: "",
        config: {
          name: "name_input",
          type: "text",
          placeholder: "Enter organisation name",
          label: "Name"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: true
      },
      hourlyRate: {
        element: "input",
        value: "",
        config: {
          name: "rate_input",
          type: "text",
          placeholder: "enter rate",
          label: "Hourly rate $"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: true
      }
    },
    serverError: null,
    isLoading: true,
    organisations: [],
    orgId: null
  };

  componentDidMount() {
    this.fetchOrganisation();
  }

  fetchOrganisation() {
    axios
      .get("/organisations", {
        headers: { Authorization: localStorage.getItem("token") }
      })
      .then(
        response => {
          this.setState({
            serverError: null,
            organisations: response.data,
            isLoading: false
          });
        },
        error => {
          this.setState({
            serverError: "Something went wrong.",
            isLoading: false
          });
        }
      );
  }

  getOrganisations = () => {
    return (
      <div>
        <h1>Organisations</h1>
        <ul>
          {this.state.isLoading ? (
            <div>fetching...</div>
          ) : (
            this.state.organisations.map(item => (
              <li key={item.id}>
                {item.name} <Link to={`/edit/${item.id}`}>Edit</Link>{" "}
                <Link to={`/organisation/join/${item.id}`}>Join</Link>
                {/* <button onClick={event => this.joinOrg(item.id)}>Join</button> */}
              </li>
            ))
          )}
        </ul>
      </div>
    );
  };

  userOrganization = () => {
    return (
      <div>
        <div>
          <p>
            You aren't member of any organisations. Join an existing one or
            create a new one.
          </p>
        </div>
        {this.getOrganisations()}

        <div>
          <h1>Create organisation</h1>
        </div>
      </div>
    );
  };

  updateForm = element => {
    const newFormdata = update(element, this.state.formdata, "create_org");
    this.setState({
      formError: false,
      formdata: newFormdata
    });
  };

  submitForm = event => {
    event.preventDefault();

    let dataToSubmit = generateData(this.state.formdata, "create_org");
    let formIsValid = isFormValid(this.state.formdata, "create_org");

    if (formIsValid) {
      axios
        .post("/organisations/create_join", dataToSubmit, {
          headers: { Authorization: localStorage.getItem("token") }
        })
        .then(
          response => {
            this.fetchOrganisation();
          },
          error => {
            this.setState({ serverError: "Something went wrong." });
          }
        );
    } else {
      this.setState({
        formError: true
      });
    }
  };

  userSelectedOrg = () => {
    const orgId = this.props.match.params.id;
    if (orgId !== undefined && !this.state.isLoading) {
      const { organisations } = this.state;

      const selectedOrg = organisations.find(
        org => parseInt(org.id) === parseInt(orgId)
      );

      return (
        <div>
          <h1>{selectedOrg.name}</h1>
          <Link to="/shifts">View Shifts</Link> {" | "}{" "}
          <Link to="/edit">Edit</Link> {" | "} <Link to="/Leave">Leave</Link>
        </div>
      );
    }
    return null;
  };

  joinOrg = orgId => {
    console.log("session id: ", localStorage.getItem("token"));
    axios
      .post(
        "organisations/join/",
        {},
        {
          headers: { Authorization: localStorage.getItem("token") }
        }
      )
      .then(
        response => {
          this.fetchOrganisation();
        },
        error => {
          this.setState({ serverError: "Something went wrong." });
        }
      );
  };

  render() {
    const userId = this.props.match.params.id;

    return (
      <div className="page_wrapper">
        <Header {...this.props} />
        {userId ? this.userSelectedOrg() : this.userOrganization()}
        <div className="container">
          <div className="register_login_container">
            <div className="">
              <form onSubmit={event => this.submitForm(event)}>
                <div className="form_block_two">
                  <div className="block">
                    <FormField
                      id={"name"}
                      formdata={this.state.formdata.name}
                      change={element => this.updateForm(element)}
                    />
                  </div>
                </div>
                <div>
                  <FormField
                    id={"hourlyRate"}
                    formdata={this.state.formdata.hourlyRate}
                    change={element => this.updateForm(element)}
                  />
                </div>

                <div>
                  {this.state.formError ? (
                    <div className="error_label">Please check your data</div>
                  ) : null}
                  <button onClick={event => this.submitForm(event)}>
                    Create and Join
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
