import React, { Component } from "react";
import { Link, Route } from "react-router-dom";
import Header from "../header/";
import FormField from "../utils/form/formField";
import { validate } from "../utils/form/misc";
import { update, generateData, isFormValid } from "../utils/form/formActions";
import axios from "axios";

export class Edit extends Component {
  state = {
    formError: false,
    formSuccess: "",
    formdata: {
      name: {
        element: "input",
        value: "",
        config: {
          name: "name_input",
          type: "text",
          placeholder: "Enter organisation name",
          label: "Name"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: true
      },
      hourlyRate: {
        element: "input",
        value: "",
        config: {
          name: "rate_input",
          type: "text",
          placeholder: "enter rate",
          label: "Hourly rate $"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: true
      }
    },
    serverError: null,
    isLoading: true,
    organisations: [],
    orgId: null
  };

  componentDidMount() {
    this.fetchOrganisation();
  }

  fetchOrganisation() {
    const orgId = this.props.match.params.id;
    axios
      .get("/organisations", {
        headers: { Authorization: localStorage.getItem("token") }
      })
      .then(
        response => {
          const org = response.data.find(
            org => parseInt(org.id) === parseInt(orgId)
          );

          const newFormdata = {
            ...this.state.formdata
          };

          const nameElement = {
            ...newFormdata["name"]
          };
          nameElement.value = org.name;
          nameElement.valid = true;
          const rateElement = {
            ...newFormdata["hourlyRate"]
          };
          rateElement.value = org.hourlyRate;
          rateElement.valid = true;

          newFormdata.name = nameElement;
          newFormdata.hourlyRate = rateElement;

          this.setState({
            serverError: null,
            organisations: org,
            isLoading: false,
            formdata: newFormdata
          });
        },
        error => {
          this.setState({
            serverError: "Something went wrong.",
            isLoading: false
          });
        }
      );
  }

  updateForm = element => {
    const newFormdata = update(element, this.state.formdata, "update_org");
    this.setState({
      formError: false,
      formdata: newFormdata
    });
  };

  submitForm = event => {
    event.preventDefault();
    const orgId = this.props.match.params.id;

    let dataToSubmit = generateData(this.state.formdata, "update_org");
    let formIsValid = isFormValid(this.state.formdata, "update_org");
    console.log("data to submit: ", dataToSubmit, "is valid: ", formIsValid);

    if (formIsValid) {
      axios
        .put(`/organisations/${orgId}`, dataToSubmit, {
          headers: { Authorization: localStorage.getItem("token") }
        })
        .then(
          response => {
            this.props.history.push("/organisation");
          },
          error => {
            this.setState({ serverError: "Something went wrong." });
          }
        );
    } else {
      this.setState({
        formError: true
      });
    }
  };

  render() {
    const { organisations, isLoading, serverError } = this.state;

    return (
      <div>
        <Header {...this.props} />

        <h1>Edit Organisation</h1>
        {isLoading ? null : (
          <div className="container">
            <div className="register_login_container">
              <div className="">
                <form onSubmit={event => this.submitForm(event)}>
                  <div className="form_block_two">
                    <div className="block">
                      <FormField
                        id={"name"}
                        formdata={this.state.formdata.name}
                        change={element => this.updateForm(element)}
                      />
                    </div>
                  </div>
                  <div>
                    <FormField
                      id={"hourlyRate"}
                      formdata={this.state.formdata.hourlyRate}
                      change={element => this.updateForm(element)}
                    />
                  </div>
                  {this.state.formError ? (
                    <div className="error_label">Please check your data</div>
                  ) : null}
                  {serverError ? (
                    <div className="error_label">{serverError}</div>
                  ) : null}
                  <div>
                    <button onClick={event => this.submitForm(event)}>
                      Update
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Edit;
