import React, { Component } from "react";
import axios from "axios";
import { Link, Route } from "react-router-dom";
import Header from "../header/";

export class Join extends Component {
  state = {
    serverError: null,
    organisations: [],
    isLoading: true
  };
  componentDidMount() {
    this.fetchOrganisation();
  }

  fetchOrganisation() {
    axios
      .get("/organisations", {
        headers: { Authorization: localStorage.getItem("token") }
      })
      .then(
        response => {
          this.setState({
            serverError: null,
            organisations: response.data,
            isLoading: false
          });
        },
        error => {
          this.setState({
            serverError: "Something went wrong.",
            isLoading: false
          });
        }
      );
  }

  getJoinedOrg = () => {
    const orgId = this.props.match.params.id;
    if (orgId !== undefined && !this.state.isLoading) {
      const { organisations } = this.state;

      const selectedOrg = organisations.find(
        org => parseInt(org.id) === parseInt(orgId)
      );

      return (
        <div>
          <h1>{selectedOrg.name}</h1>
          <Link to="/shifts">View Shifts</Link> {" | "}{" "}
          <Link to={`/edit/${orgId}`}>Edit</Link> {" | "}{" "}
          <Link to="/organisation">Leave</Link>
        </div>
      );
    }
    return null;
  };

  render() {
    const orgId = this.props.match.params.id;
    return (
      <div className="page_wrapper">
        <Header {...this.props} />
        {orgId ? this.getJoinedOrg() : null}
      </div>
    );
  }
}

export default Join;
