import React, { Component } from "react";
import { Link, Route } from "react-router-dom";
import Header from "../header/";
import FormField from "../utils/form/formField";
import { update, generateData, isFormValid } from "../utils/form/formActions";
import axios from "axios";

export class Shift extends Component {
  state = {
    formError: false,
    formSuccess: "",
    formdata: {
      shiftDate: {
        element: "input",
        value: "",
        config: {
          name: "shiftDate_input",
          type: "text",
          placeholder: "Shift date",
          label: "date"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: false
      },
      startTime: {
        element: "input",
        value: "",
        config: {
          name: "startTime_input",
          type: "text",
          placeholder: "Start time",
          label: "Hourly rate $"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: false
      },
      finishTime: {
        element: "input",
        value: "",
        config: {
          name: "finishTime_input",
          type: "text",
          placeholder: "Finish time",
          label: ""
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: false
      },
      breakDuration: {
        element: "input",
        value: "",
        config: {
          name: "breakDuration_input",
          type: "text",
          placeholder: "Break duration",
          label: "Hourly rate $"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: false
      },
      hoursWorked: {
        element: "input",
        value: "",
        config: {
          name: "hoursWorked_input",
          type: "text",
          placeholder: "Worked hours",
          label: "Hourly rate $"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: false
      },
      shiftCost: {
        element: "input",
        value: "",
        config: {
          name: "shiftCost_input",
          type: "text",
          placeholder: "Shift cost",
          label: "Hourly rate $"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: false
      }
    },
    serverError: null,
    isLoading: true
  };

  updateForm = element => {
    const newFormdata = update(element, this.state.formdata, "shift_create");
    this.setState({
      formError: false,
      formdata: newFormdata
    });
  };

  submitForm = event => {
    event.preventDefault();
    const orgId = this.props.match.params.id;

    let dataToSubmit = generateData(this.state.formdata, "update_org");
    let formIsValid = isFormValid(this.state.formdata, "update_org");
    console.log("data to submit: ", dataToSubmit, "is valid: ", formIsValid);

    if (formIsValid) {
      // axios
      //   .put(`/organisations/${orgId}`, dataToSubmit, {
      //     headers: { Authorization: localStorage.getItem("token") }
      //   })
      //   .then(
      //     response => {
      //       this.props.history.push("/organisation");
      //     },
      //     error => {
      //       this.setState({ serverError: "Something went wrong." });
      //     }
      //   );
    } else {
      this.setState({
        formError: true
      });
    }
  };
  render() {
    return (
      <div>
        <Header {...this.props} />
        <h3>Shifts</h3>
        <table>
          <thead>
            <tr>
              <th>Employee name</th>
              <th>Shift date</th>
              <th>Start time</th>
              <th>Finish time</th>
              <th>Break length(minutes)</th>
              <th>Hours worked</th>
              <th>Shift cost</th>
            </tr>
          </thead>
          <tbody>
            {/* <form onSubmit={event => this.submitForm(event)}> */}
            <tr>
              <td>Emp name</td>
              <td>
                <FormField
                  id={"shiftDate"}
                  formdata={this.state.formdata.shiftDate}
                  change={element => this.updateForm(element)}
                />
              </td>
              <td>
                <FormField
                  id={"startTime"}
                  formdata={this.state.formdata.startTime}
                  change={element => this.updateForm(element)}
                />
              </td>
              <td>
                <FormField
                  id={"finishTime"}
                  formdata={this.state.formdata.finishTime}
                  change={element => this.updateForm(element)}
                />
              </td>
              <td>
                <FormField
                  id={"breakDuration"}
                  formdata={this.state.formdata.breakDuration}
                  change={element => this.updateForm(element)}
                />
              </td>
              <td>
                <button onClick={event => this.submitForm(event)}>
                  Create Shift
                </button>
              </td>
            </tr>
            {/* </form> */}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Shift;
