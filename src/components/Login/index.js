import React, { Component } from "react";
import FormField from "../utils/form/formField";
import { validate } from "../utils/form/misc";
import { update, generateData, isFormValid } from "../utils/form/formActions";
import axios from "axios";

import { Link } from "react-router-dom";

class Login extends Component {
  state = {
    formError: false,
    formSuccess: "",
    formdata: {
      email: {
        element: "input",
        value: "",
        config: {
          name: "email_input",
          type: "email",
          placeholder: "Enter your email",
          label: "Email"
        },
        validation: {
          required: true,
          email: true
        },
        valid: false,
        validationMessage: "",
        showlabel: true
      },
      password: {
        element: "input",
        value: "",
        config: {
          name: "password_input",
          type: "password",
          placeholder: "Enter your password",
          label: "Password"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: "",
        showlabel: true
      }
    },
    isChecked: false,
    serverError: null
  };

  updateForm(element) {
    const newFormdata = { ...this.state.formdata };
    const newElement = { ...newFormdata[element.id] };

    newElement.value = element.event.target.value;

    let validData = validate(newElement);
    newElement.valid = validData[0];
    newElement.validationMessage = validData[1];

    newFormdata[element.id] = newElement;

    this.setState({
      formError: false,
      formdata: newFormdata
    });
  }

  submitForm(event) {
    event.preventDefault();

    let dataToSubmit = generateData(this.state.formdata, "login");
    let formIsValid = isFormValid(this.state.formdata, "login");

    if (formIsValid) {
      axios.post("/auth/login", dataToSubmit).then(
        response => {
          localStorage.setItem("token", response.data.sessionId);
          localStorage.setItem("name", dataToSubmit.email);
          this.props.history.push("/organisation");
        },
        error => {
          this.setState({ serverError: "Something went wrong." });
        }
      );
    } else {
      this.setState({
        formError: true
      });
    }
  }

  render() {
    const { error } = this.state;

    return (
      <div className="container">
        <div className="signin_wrapper" style={{ margin: "100px" }}>
          <form onSubmit={event => this.submitForm(event)}>
            <h2>Log in</h2>

            <FormField
              id={"email"}
              formdata={this.state.formdata.email}
              change={element => this.updateForm(element)}
            />

            <FormField
              id={"password"}
              formdata={this.state.formdata.password}
              change={element => this.updateForm(element)}
            />

            <div
              style={{
                display: "flex",
                alignItems: "center"
              }}
            >
              <input
                name="isChecked"
                type="checkbox"
                value={this.state.isChecked}
                onChange={event => this.updateForm("engineer", event)}
                style={{
                  width: "24px",
                  height: "24px"
                }}
              />
              <p>Remember me</p>
            </div>

            {this.state.formError ? (
              <div className="error_label">Please check you're data.</div>
            ) : null}
            {this.state.serverError ? (
              <div className="error_label">{this.state.serverError}</div>
            ) : null}
            <button onClick={event => this.submitForm(event)}>Login</button>
          </form>
          <div>
            <Link to="/signup">Sign up</Link>
          </div>
          <div>
            <Link to="/">Forgot your password?</Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
