import React, { Component } from "react";
import { Link } from "react-router-dom";

const unAuthenticated = () => <div>login in first</div>;

class Header extends Component {
  state = {
    isLoading: true,
    token: null
  };

  componentDidMount() {
    this.setState({ token: localStorage.getItem("token"), isLoading: false });
  }

  logoutUser() {
    // const request = axios.get(`${USER_SERVER}/logout`)
    // .then(response => response.data);

    // return {
    //     type: LOGOUT_USER,
    //     payload: request
    // }
    console.log("logout user: ", this.props);
    localStorage.clear();
    this.props.history.push("/");
  }

  navComponent = () => {
    const { token } = this.state;
    if (token) {
      return (
        <div>
          <Link to="/organisation">Adnat</Link>
          <p>
            Logged in as {localStorage.getItem("name")}{" "}
            <button onClick={() => this.logoutUser()}>logout</button>
          </p>
        </div>
      );
    } else {
      return null;
    }
  };

  render() {
    return <div>{this.state.isLoading ? null : this.navComponent()}</div>;
  }
}

export default Header;
