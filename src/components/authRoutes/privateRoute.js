import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";

const PrivateRoutes = ({ component: Comp, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        localStorage.getItem("token") ? (
          <Comp {...props} />
        ) : (
          <Redirect
            to={{ pathname: "/login", state: { from: props.location } }}
          />
        )
      }
    />
  );
};

export default PrivateRoutes;
