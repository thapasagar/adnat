import React, { Component } from "react";
import Header from "../../components/header";

class Layout extends Component {
  render() {
    console.log("props --- : ", this.props.history);
    return (
      <div>
        {/* <Header/> */}
        <div className="page_container">{this.props.children}</div>
      </div>
    );
  }
}

export default Layout;
// const Layout = props => {
//   console.log("props --- : ", props.history);
//   return (
//     <div>
//       <Header {...props} />
//       {props.children}
//     </div>
//   );
// };

// export default Layout;
